var express = require('express'); // poziva express iz node_modules
var app = express(); // označava da naša aplikacija koristi express
var mysql = require('mysql'); // poziva mysql iz node_modules
var cors = require('cors')
var bodyParser = require('body-parser'); // poziva bodyParser iz node_modules
var moment = require('moment'); //Moment.js za upravljanje vremenom

// označava da app koristi bodyParser() kako bi mogao dobiti podatke iz POST zahtjeva
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(cors())
// definira port na kojemu će API raditi
var port = process.env.PORT || 8080;

// Spajanje s MySQL bazom
var pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'fitness'
});

// definiranje zadane (defaultne) rute za naš API
var apiRoutes = express.Router();

// middleware
apiRoutes.use(function (req, res, next) {
  // provjera korisničkih podataka prilikom logirana/prijave
  console.log('Nešto se događa.');
  next(); // idi na sljedeću rutu
});

// testiranje rute (GET http://localhost:8080/api)
apiRoutes.get('/', function (req, res) {
  //ako je sve ispravno postavljeno kao odgovor ćemo dobiti ovu poruku
  res.json({
    message: 'API radi!'
  });
});



//Dodavanje korisnika
apiRoutes.post('/dodajkorisnika', function (req, res, next) {

  pool.getConnection(function (err, connection) {

    if (err) {
      console.error("Dogodila se greška: " + err);
    }

    var trenutnoVrijeme = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');

    var korisnici = {
      email: req.body.email,
      lozinka: req.body.lozinka,
      timestampCreatedAt: trenutnoVrijeme
    };

    connection.query('INSERT INTO korisnici SET ?', korisnici,
      function (err, rows) {
        if (err) {
          throw err;
        } else {
          res.json("Uspješno dodan korisnik!");
          res.end();
        }
        connection.release();
      });
  });
});

//API za dodavanje bloga
apiRoutes.post('/dodajBlog', function (req, res, next) {

  pool.getConnection(function (err, connection) {

    if (err) {
      console.error("Dogodila se greška: " + err);
    }

    var trenutnoVrijeme = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');


    var blog = {
      naslov: req.body.naslov,
      kategorija: req.body.kategorija,
      tekst: req.body.blogTekst,
      timestampCreatedAt: trenutnoVrijeme
    };

    connection.query('INSERT INTO blog SET ?', blog,
      function (err, rows) {
        if (err) {
          throw err;
        } else {
          res.json("Uspješno dodan novi blog!");
          res.end();
        }
        connection.release();
      });
  });
});

//Dohvačanje bloga po ID-u
apiRoutes.post('/dohvatiBlog', function (req, res, next) {

  pool.getConnection(function (err, connection) {
    if (err) {
      console.error("Dogodila se greška: " + err);
    }
    let query = 'SELECT * FROM blog WHERE blog_id = ?';

    var table = [req.body.blog_id];

    query = mysql.format(query, table);

    connection.query(query,
      function (err, rows) {
        if (err) {
          return next(err);
        } else {
          if (rows.length === 0) {
            res.json({
              success: false,
              message: "Ne postoji blog s tim ID-em"
            })
          } else {
            res.json({
              success: true,
              message: "Uspješno dohvačen blog",
              popis_korisnika: rows
            });
          }

        }
      });

  });
}); 

//API za dohvačanje svih blogova
apiRoutes.get('/pregledBlogova', function (req, res, next) {

  pool.getConnection(function (err, connection) {

    if (err) {
      console.error("Dogodila se greška: " + err);
    }

    var query = "SELECT *, DATE_FORMAT(timestampCreatedAt,'%d.%m.%Y') AS niceDate FROM blog ORDER BY timestampCreatedAt DESC";

    query = mysql.format(query);

    connection.query(query, function (err, rows) {
      connection.release();
      if (err) {
        return next(err);
      } else {
        res.json({
          success: true,
          popis_korisnika: rows
        });
      }
    });
  });
});

//Dohvačanje korisnika po emailu i passwordu odnosno prijava korisnika
apiRoutes.post('/prijava', function (req, res, next) {

  pool.getConnection(function (err, connection) {
    if (err) {
      console.error("Dogodila se greška: " + err);
    }
    let query = 'SELECT * FROM korisnici WHERE email = ? AND lozinka = ?';

    var table = [req.body.email, req.body.lozinka];

    query = mysql.format(query, table);
    connection.query(query,
      function (err, rows) {
        if (err) {
          return next(err);
        } else {
          //res.json("uspješan login");
          if (rows.length === 0) {
            res.json({
              success: false,
              message: "Kriva lozinka ili korisnik ne postoji."
            })
          } else {
            res.json({
              success: true,
              message: "Uspješan login",
              popis_korisnika: rows
            });
          }

        }
      });

  });
});

//Dohvaćanje svih korisnika
apiRoutes.get('/pregledkorisnika', function (req, res, next) {

  pool.getConnection(function (err, connection) {

    if (err) {
      console.error("Dogodila se greška: " + err);
    }

    var query = "SELECT * FROM korisnici ORDER BY k_id ASC";

    var table = ["fitness"];

    query = mysql.format(query, table);

    connection.query(query, function (err, rows) {
      connection.release();
      if (err) {
        return next(err);
      } else {
        res.json({
          success: true,
          popis_korisnika: rows
        });
      }
    });
  });
});



//Uređivanje korisnika
apiRoutes.put('/korisnici/:k_id', function (req, res, next) {

  pool.getConnection(function (err, connection) {

    if (err) {
      console.error("Dogodila se greška: " + err);
    }

    var korisnici = {
      email: req.body.email,
      lozinka: req.body.lozinka
    };

    connection.query('update korisnici SET ? where k_id = ?', [korisnici, req.params.k_id],
      function (err, rows) {
        if (err) {
          return next(err);
        } else {
          res.writeHead(200, {
            "Content-Type": "application/json"
          });
          var result = {
            success: true,
            detail: rows
          }
          res.write(JSON.stringify(result));
          res.end();
        }
      });
  });
});

//Brisanje korisnika
apiRoutes.delete('/korisnici/:k_id', function (req, res, next) {

  pool.getConnection(function (err, connection) {

    if (err) {
      console.error("Dogodila se greška: " + err);
    }

    connection.query('delete from korisnici where k_id = ?', [req.params.k_id],

      function (err, rows) {
        if (err) {
          return next(err);
        } else {
          res.writeHead(200, {
            "Content-Type": "application/json"
          });
          var result = {
            success: true
          }
          res.write(JSON.stringify(result));
          res.end();
        }
      });
  });
});

// sve rute sadržavati će /api
app.use('/api', apiRoutes);

// pokretanje API-ja
app.listen(port);
console.log('API je pokrenut i koristi port:' + ' ' + port);
