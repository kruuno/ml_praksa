import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class KorisniciAPIService {

  constructor(private http: HttpClient) { }

  url: string = "http://localhost:8080/api/"

  GetKorisnici(){
    return this.http.get(this.url + 'pregledkorisnika').
    pipe(
    map(data => Object.keys(data).map(k => data[k]))
    );
  }

  GetBlogove(){
    return this.http.get(this.url + 'pregledBlogova').
    pipe(
    map(data => Object.keys(data).map(k => data[k]))
    );
  }

  dohvatiBlogPoID(a:any){
    return this.http.post(this.url + 'dohvatiBlog', a).
    pipe(
      map(data => Object.keys(data).map(k => data[k]))
    );
  }

  dodajBlog(a:any){
    return this.http.post(this.url + 'dodajBlog', a).
    pipe(
      map(data => Object.keys(data).map(k => data[k]))
    );
  }

  dodaj(a:any){
    return this.http.post(this.url + 'dodajkorisnika', a).
    pipe(
      map(data => Object.keys(data).map(k => data[k]))
    );
  }


  prijava(a:any){
    return this.http.post(this.url + 'prijava', a).
    pipe(
      map(data => Object.keys(data).map(k => data[k]))
    );
  }


}
