import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { KorisniciAPIService } from '../korisnici-api.service';
import { stringify } from 'querystring';

@Component({
  selector: 'app-prijava',
  templateUrl: './prijava.component.html',
  styleUrls: ['./prijava.component.scss']
})


export class PrijavaComponent implements OnInit {

  k_ime:string;
  lozinka:string;
  korisnici: any = [];

  constructor(private router: Router, private korisniciapiservice: KorisniciAPIService) { }

  ngOnInit() {
      this.dohvatiSveKorisnike();
  }

  Prijava(){
    if(this.k_ime == "Admin" && this.lozinka == "admin"){
      console.log("Uspješna prijava");
      this.router.navigate(['/']);
    }else{
      console.log("Netočni podaci");
    }
  }


  //Dohvaća sve korisnike iz API-ja s funkcijom GetKorisnici().
  dohvatiSveKorisnike(){
    return this.korisniciapiservice.GetKorisnici().subscribe(res => {
      console.log(res[1]);
      this.korisnici = res[1];
     });
   }

}
