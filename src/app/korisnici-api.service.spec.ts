import { TestBed } from '@angular/core/testing';

import { KorisniciAPIService } from './korisnici-api.service';

describe('KorisniciAPIService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KorisniciAPIService = TestBed.get(KorisniciAPIService);
    expect(service).toBeTruthy();
  });
});
