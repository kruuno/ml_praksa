import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-registracija',
  templateUrl: './registracija.component.html',
  styleUrls: ['./registracija.component.scss']
})
export class RegistracijaComponent implements OnInit {

  ime:string;
  prezime:string;
  k_ime:string;
  lozinka:string;

  constructor(private router: Router) {  }

  ngOnInit() {
  }

  Registracija(){
    
    console.log("Vaše korisničko ime je: " + this.k_ime +", a lozinka: "+this.lozinka);
    this.router.navigate(['/prijava']);
  }

}
