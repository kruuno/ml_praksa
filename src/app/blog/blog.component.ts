import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { KorisniciAPIService } from '../korisnici-api.service'
import $ from 'jquery';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  BlogText: string;
  blogovi: any = [];
  blog: any = [];
  constructor(private router: Router, private korisniciapiservice: KorisniciAPIService) { }

  ngOnInit() {
    this.dohvatiSveBlogove();
    

    let addPostBtn = $('#addPostBtn');
    let blogPosts = $('#blogPosts');
    let newBlog =$('#newBlog');
    let footer = $('#footer');
    addPostBtn.on('click', function() {
      blogPosts.addClass('inactive');
      footer.addClass('inactive');
      newBlog.removeClass('inactive');
    });
  }

  dodajBlog(blog){
    console.log(blog.value);
    this.korisniciapiservice.dodajBlog(blog.value).subscribe(
      res => {
        console.log(res);
      });
  }
 

  dohvatiSveBlogove(){
    return this.korisniciapiservice.GetBlogove().subscribe(res => {
      console.log(res[1]);
      this.blogovi = res[1];
     });
   }

}
