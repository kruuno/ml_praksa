import { Component, OnInit } from '@angular/core';
import $ from 'jquery';
import { Router } from '@angular/router';
import { KorisniciAPIService } from '../korisnici-api.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  email: string;
  lozinka: string;
  korisnik: any = [];
  error: string;


  constructor(private router: Router, private korisniciapiservice: KorisniciAPIService) {

   }

  ngOnInit() {
    let home = $('#home');
    let prijava = $('#prijava');
    let prijavaBtn = $('#prijavaBtn');
    let registracijaBtn = $('#registracijaBtn');
    let registracija = $('#registracija');
    let closeBtn = $('.closeBtn');
    prijavaBtn.on('click', function() {
      home.removeClass('active');
      registracija.removeClass('active');
      prijava.addClass('active');
    });

    registracijaBtn.on('click', function() {
      home.removeClass('active');
      registracija.addClass('active');
      prijava.removeClass('active');
    });

    closeBtn.on('click', function() {
      prijava.removeClass('active');
      registracija.removeClass('active');
      home.addClass('active');
    });

  }


  Prijava(korisnik){
    console.log(korisnik.value);
    this.korisniciapiservice.prijava(korisnik.value).subscribe(
      res => {
        console.log(res);
        if(res[0] === true){
          sessionStorage.setItem('email', korisnik.value.email);
          
          window.location.reload();
        }else{
          // alert(res[1]);
          this.error = res[1];
        }

      });
  }
//registracija
  dodajKorisnika(korisnik){
    console.log(korisnik.value);
    this.korisniciapiservice.dodaj(korisnik.value).subscribe(
     res => {
       console.log(res);
     });
 }
 
}
