import { Component } from '@angular/core';
import { environment } from './../environments/environment';
import $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  myItem = sessionStorage.getItem("email");
  constructor(){
    
    //Dohvacanje teksta ispred emaila
    if(this.myItem){
      this.myItem.toString();
      this.myItem = this.myItem.split('@')[0];
    }
    
  }
  title = 'Fitness';
  

  odjavaKorisnika(){
    console.log("test");
    console.log(this.myItem);
    sessionStorage.removeItem("email");
    window.location.reload();
  }

}
