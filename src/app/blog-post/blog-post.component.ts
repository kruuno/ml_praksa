import { Component, OnInit } from '@angular/core';
import $ from 'jquery';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { KorisniciAPIService } from '../korisnici-api.service'
import 'magnific-popup';

@Component({
  selector: 'app-blog-post',
  templateUrl: './blog-post.component.html',
  styleUrls: ['./blog-post.component.scss']
})
export class BlogPostComponent implements OnInit {

  blog_id: any;
  blog: any = {};
 
  constructor(private router: Router, private route: ActivatedRoute, private korisniciapiservice: KorisniciAPIService) {
   
   }

  ngOnInit() {
    this.blog_id=this.route.snapshot.paramMap.get("id");
    let body = { blog_id: this.blog_id }
    console.log(body);

    this.dohvatiBlog(body);

    $('.imgList').magnificPopup({
      delegate: 'a', // child items selector, by clicking on it popup will open
      gallery: {
          enabled:true
      },
      type: 'image'
      // other options
    });
  } 

  dohvatiBlog(blog_id){
    this.korisniciapiservice.dohvatiBlogPoID(blog_id).subscribe(
      res => {
        console.log(res);
        this.blog = res[2];
      });
  }

}
